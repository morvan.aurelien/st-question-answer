<?php

namespace App\Tests\UnitTests\Helpers;

use App\Entity\Question;
use App\Helpers\ExportableEntityService;
use App\Inputs\AnswerInput;
use App\Inputs\QuestionInput;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ExportableEnttiyServiceTest extends KernelTestCase
{
    protected ExportableEntityService $service;

    protected function setUp(): void
    {
        self::bootKernel();
        $container = static::getContainer();

        $this->service = $container->get(ExportableEntityService::class);
    }

    public function testExportOnSuccess()
    {
        $questionInput = new QuestionInput();
        $questionInput->setTitle('First question');
        $questionInput->setStatus('draft');
        $questionInput->setPromoted(false);
        $answerInput = new AnswerInput();
        $answerInput->setChannel('faq');
        $answerInput->setBody('Content answer');
        $questionInput->setAnswers([$answerInput]);
        $question = Question::createFromInput($questionInput);

        $datasExportToCsv = $this->service->export($question);

        static::assertEquals('string', gettype($datasExportToCsv));
    }
}
