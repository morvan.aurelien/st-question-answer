<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Nelmio\Alice\Loader\NativeLoader;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractApiTestCase extends WebTestCase
{
    protected EntityManagerInterface $entityManager;

    protected ?KernelBrowser $clientApi;

    protected function setUp(): void
    {
        $this->clientApi = self::createClient();
        self::bootKernel();
        $container = static::getContainer();
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
        $schemaTool = new SchemaTool($this->entityManager);

        $schemaTool->dropSchema($this->entityManager->getMetadataFactory()->getAllMetadata());
        $schemaTool->createSchema($this->entityManager->getMetadataFactory()->getAllMetadata());
    }

    protected function request(string $method, string $uri, string $payload = null): Response
    {
        $this->clientApi->request(
            $method,
            $uri,
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            $payload
        );

        return $this->clientApi->getResponse();
    }

    protected function loadFixturs(string $pathToFixtures): void
    {
        $loader = new NativeLoader();
        $objectSet = $loader->loadFile(__DIR__.'/fixtures/'.$pathToFixtures);
        foreach ($objectSet->getObjects() as $object) {
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
    }
}
