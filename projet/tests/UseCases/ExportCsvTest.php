<?php

namespace App\Tests\UseCases;

use App\Entity\HistoricQuestion;
use App\Entity\Question;
use App\Inputs\AnswerInput;
use App\Inputs\QuestionInput;
use App\Tests\AbstractApiTestCase;

class ExportCsvTest extends AbstractApiTestCase
{
    private const URI = '/api/exports';

    public function testExportCsvWithoutType()
    {
        $response = $this->request(
            'GET',
            self::URI
        );

        $jsonExpected = <<<EOF
{
    "message": "You must define type of datas targeted. Allow `question` or `historic`"
}
EOF;

        static::assertEquals(400, $response->getStatusCode());
        static::assertJsonStringEqualsJsonString($jsonExpected, $response->getContent());
    }

    public function testExportCsvWithInvalidType()
    {
        $response = $this->request(
            'GET',
            self::URI.'?type=invalid'
        );

        $jsonExpected = <<<EOF
{
    "message": "Type not allowed. Allow historic,question"
}
EOF;
        static::assertEquals(400, $response->getStatusCode());
        static::assertJsonStringEqualsJsonString($jsonExpected, $response->getContent());
    }

    public function testSuccessfulExportQuestion()
    {
        $this->createQuestion();
        $response = $this->request(
            'GET',
            self::URI.'?type=question'
        );

        static::assertTrue($response->headers->has('content-disposition'));
        static::assertEquals(200, $response->getStatusCode());
    }

    public function testSuccessfulExportHistoric()
    {
        $question = $this->createQuestion();
        $historic = new HistoricQuestion($question, 'olddatas', 'newdatas');
        $this->entityManager->persist($historic);
        $this->entityManager->flush();

        $response = $this->request(
            'GET',
            self::URI.'?type=historic'
        );
        static::assertTrue($response->headers->has('content-disposition'));
        static::assertEquals(200, $response->getStatusCode());
    }

    private function createQuestion()
    {
        $questionInput = new QuestionInput();
        $questionInput->setTitle('First question');
        $questionInput->setStatus('draft');
        $questionInput->setPromoted(false);
        $answerInput = new AnswerInput();
        $answerInput->setChannel('faq');
        $answerInput->setBody('Content answer');
        $questionInput->setAnswers([$answerInput]);
        $question = Question::createFromInput($questionInput);
        $this->entityManager->persist($question);
        $this->entityManager->flush();

        return $question;
    }
}
