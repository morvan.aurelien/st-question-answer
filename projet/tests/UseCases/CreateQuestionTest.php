<?php

namespace App\Tests\UseCases;

use App\Entity\Question;
use App\Tests\AbstractApiTestCase;

class CreateQuestionTest extends AbstractApiTestCase
{
    private const URI = '/api/questions';

    public function testWithMissingMandatoryField()
    {
        $response = $this->request(
            'POST',
            self::URI,
            json_encode([])
        );

        $jsonExpected = <<<EOF
{
    "promoted": [
        "Ce champ ne peut être null."
    ],
    "status": [
        "Le status est requis."
    ],
    "title": [
        "Le titre est requis."
    ],
    "answers": [
        "La question doit contenir au moins 1 réponse."
    ]
}
EOF;

        static::assertEquals(400, $response->getStatusCode());
        static::assertJsonStringEqualsJsonString($jsonExpected, $response->getContent());
    }

    public function testWithFieldIsTooLong()
    {
        $response = $this->request(
            'POST',
            self::URI,
            json_encode(
                [
                    "title" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quis sapien viverra, volutpat purus id, scelerisque donec.",
                    "promoted" => false,
                    "status" => "draft",
                    "answers" => [
                        [
                            "channel" => "bot",
                            "body" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel nisl dictum, bibendum ex non, tempus elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam mi quam, elementum ut orci eu, dictum viverra mauris. Donec tincidunt id libero id placerat. Morbi non justo lectus. Donec commodo pulvinar ligula vel semper. Morbi molestie in tellus vulputate pretium. Curabitur pellentesque sagittis orci, ac dictum eros placerat ac. Sed fringilla massa at blandit."
                        ]
                    ]
                ]
            )
        );
        $jsonExpected = <<<EOF
{
    "title": [
        "Le titre ne doit pas faire plus de 100 caractères."
    ],
    "answers[0].body": [
        "Longueur maximum autorisé, 500 caractères."
    ]
}
EOF;

        static::assertEquals(400, $response->getStatusCode());
        static::assertJsonStringEqualsJsonString($jsonExpected, $response->getContent());
    }

    public function testWithInvalidChoiceOnStatusAndChannel()
    {
        $response = $this->request(
            'POST',
            self::URI,
            json_encode(
                [
                    "title" => "Lorem ipsum dolor sit amet",
                    "promoted" => false,
                    "status" => "open",
                    "answers" => [
                        [
                            "channel" => "mirc",
                            "body" => "Lorem ipsum dolor sit amet."
                        ]
                    ]
                ]
            )
        );
        $jsonExpected = <<<EOF
{
    "status": [
        "Merci de choisir un status valid `draft` or `published`"
    ],
    "answers[0].channel": [
        "Merci de choisir un channel valide. `faq` or `bot` sont autorisés."
    ]
}
EOF;
        static::assertEquals(400, $response->getStatusCode());
        static::assertJsonStringEqualsJsonString($jsonExpected, $response->getContent());
    }

    public function testSuccessfulCreateQuestion()
    {
        $actualQuestions = $this->entityManager->getRepository(Question::class)->findAll();
        static::assertCount(0, $actualQuestions);
        $response = $this->request(
            'POST',
            self::URI,
            json_encode(
                [
                    "title" => "Lorem ipsum dolor sit amet",
                    "promoted" => false,
                    "status" => "draft",
                    "answers" => [
                        [
                            "channel" => "faq",
                            "body" => "Lorem ipsum dolor sit amet."
                        ]
                    ]
                ]
            )
        );
        $questionsIntoDatabase = $this->entityManager->getRepository(Question::class)->findAll();
        static::assertEquals(201, $response->getStatusCode());
        static::assertCount(1, $questionsIntoDatabase);
    }
}
