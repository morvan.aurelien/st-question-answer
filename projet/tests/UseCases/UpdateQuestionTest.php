<?php

namespace App\Tests\UseCases;

use App\Entity\Answer;
use App\Entity\Question;
use App\Inputs\AnswerInput;
use App\Inputs\QuestionInput;
use App\Tests\AbstractApiTestCase;

class UpdateQuestionTest extends AbstractApiTestCase
{
    const URL = '/api/questions/:id';

    public function testUpdateOnNotFoundQuestion()
    {
        $response = $this->request(
            'PUT',
            str_replace(':id', 'fakeid', self::URL),
            json_encode([])
        );
        $jsonExpected = <<<EOF
{
    "message": "La question n'a pas été trouvé."
}
EOF;
        static::assertJsonStringEqualsJsonString($jsonExpected, $response->getContent());
        static::assertEquals(404, $response->getStatusCode());
    }

    public function testUpdateWithInvalidStatusAndValidTitle()
    {
        $question = $this->createQuestion();
        $response = $this->request(
            'PUT',
            str_replace(':id', $question->getId(), self::URL),
            json_encode([
                'title' => 'nouveau titre',
                'status' => 'fake_status'
            ])
        );
        $jsonExpected = <<<EOF
{
    "status": [
        "Merci de choisir un status valid `draft` or `published`"
    ]
}
EOF;
        static::assertEquals(400, $response->getStatusCode());
        static::assertJsonStringEqualsJsonString($jsonExpected, $response->getContent());
    }

    public function testUpdateWithValidStatusAndTooLongTitle()
    {
        $question = $this->createQuestion();
        $response = $this->request(
            'PUT',
            str_replace(':id', $question->getId(), self::URL),
            json_encode([
                'title' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quis sapien viverra, volutpat purus id, scelerisque donec.',
                'status' => 'published'
            ])
        );
        $jsonExpected = <<<EOF
{
    "title": [
        "Le titre ne doit pas faire plus de 100 caractères."
    ]
}
EOF;
        static::assertEquals(400, $response->getStatusCode());
        static::assertJsonStringEqualsJsonString($jsonExpected, $response->getContent());
    }

    public function testSuccessfulUpdateOnTitleFieldOnly()
    {
        $question = $this->createQuestion();
        $response = $this->request(
            'PUT',
            str_replace(':id', $question->getId(), self::URL),
            json_encode([
                'title' => 'nouveau titre'
            ])
        );

        $datasResponse = json_decode($response->getContent(), true);
        static::assertEquals(200, $response->getStatusCode());
        static::assertArrayHasKey('updated_at', $datasResponse);
    }

    public function testSuccessfulUpdateOnTitleAndStatusField()
    {
        $question = $this->createQuestion();
        $response = $this->request(
            'PUT',
            str_replace(':id', $question->getId(), self::URL),
            json_encode([
                'title' => 'nouveau titre',
                'status' => 'published'
            ])
        );

        $datasResponse = json_decode($response->getContent(), true);
        static::assertEquals(200, $response->getStatusCode());
        static::assertArrayHasKey('updated_at', $datasResponse);
    }

    private function createQuestion(): Question
    {
        $questionInput = new QuestionInput();
        $questionInput->setTitle('First question');
        $questionInput->setStatus('draft');
        $questionInput->setPromoted(false);
        $answerInput = new AnswerInput();
        $answerInput->setChannel('faq');
        $answerInput->setBody('Content answer');
        $questionInput->setAnswers([$answerInput]);
        $question = Question::createFromInput($questionInput);
        $this->entityManager->persist($question);
        $this->entityManager->flush();

        return $question;
    }
}
