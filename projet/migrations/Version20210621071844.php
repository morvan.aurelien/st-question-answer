<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210621071844 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE historic_question DROP FOREIGN KEY FK_D247F9B11E27F6BF');
        $this->addSql('ALTER TABLE historic_question ADD CONSTRAINT FK_D247F9B11E27F6BF FOREIGN KEY (question_id) REFERENCES question (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE historic_question DROP FOREIGN KEY FK_D247F9B11E27F6BF');
        $this->addSql('ALTER TABLE historic_question ADD CONSTRAINT FK_D247F9B11E27F6BF FOREIGN KEY (question_id) REFERENCES historic_question (id)');
    }
}
