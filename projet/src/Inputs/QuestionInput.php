<?php

namespace App\Inputs;

use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Question;

final class QuestionInput
{
    /**
     * @var string|null
     *
     * @Assert\NotBlank(message="Le titre est requis.", groups={"create"})
     * @Assert\Length(
     *     max=100,
     *      maxMessage="Le titre ne doit pas faire plus de {{ limit }} caractères.", groups={"create", "update"}
     * )
     */
    protected ?string $title = null;

    /**
     * @var bool|null
     *
     * @Assert\NotNull(message="Ce champ ne peut être null.", groups={"create"})
     */
    protected ?bool $promoted = null;

    /**
     * @var string|null
     *
     * @Assert\NotBlank(message="Le status est requis.", groups={"create"})
     * @Assert\Choice(
     *     choices=Question::ALLOWED_STATUS,
     *     message="Merci de choisir un status valid `draft` or `published`",
     *     groups={"create", "update"}
     * )
     */
    protected ?string $status = null;

    /**
     * @var AnswerInput[]
     *
     * @Assert\Valid(groups={"create"})
     * @Assert\Count(min=1, minMessage="La question doit contenir au moins 1 réponse.", groups={"create"})
     */
    protected array $answers = [];

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return bool|null
     */
    public function getPromoted(): ?bool
    {
        return $this->promoted;
    }

    /**
     * @param bool|null $promoted
     */
    public function setPromoted(?bool $promoted): void
    {
        $this->promoted = $promoted;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return AnswerInput[]|array
     */
    public function getAnswers(): array
    {
        return $this->answers;
    }

    /**
     * @param AnswerInput[] $answers
     */
    public function setAnswers(array $answers): void
    {
        $this->answers = $answers;
    }
}
