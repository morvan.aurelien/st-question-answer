<?php

namespace App\Inputs;

use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Answer;

final class AnswerInput
{
    /**
     * @var string|null
     *
     * @Assert\NotBlank(message="Le channel est requis.", groups={"create"})
     * @Assert\Choice(
     *     choices=Answer::ALLOWED_CHANNEL,
     *     message="Merci de choisir un channel valide. `faq` or `bot` sont autorisés.",
     *     groups={"create"}
     * )
     */
    protected ?string $channel;

    /**
     * @var string|null
     *
     * @Assert\NotBlank(message="Le corps de la réponse est requis.", groups={"create"})
     * @Assert\Length(max=500, maxMessage="Longueur maximum autorisé, {{ limit }} caractères.", groups={"create"})
     */
    protected ?string $body;

    /**
     * @return string|null
     */
    public function getChannel(): ?string
    {
        return $this->channel;
    }

    /**
     * @param string|null $channel
     */
    public function setChannel(?string $channel): void
    {
        $this->channel = $channel;
    }

    /**
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @param string|null $body
     */
    public function setBody(?string $body): void
    {
        $this->body = $body;
    }
}
