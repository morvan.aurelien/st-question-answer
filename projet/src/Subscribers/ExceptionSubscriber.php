<?php

namespace App\Subscribers;

use App\Exceptions\NotExportableEntityException;
use App\Exceptions\ValidationException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onException',
        ];
    }

    public function onException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $statusCode = $exception->getCode() > 0 ? $exception->getCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
        $datas = $exception->getMessage();
        switch (get_class($exception)) {
            case ValidationException::class:
                $statusCode = Response::HTTP_BAD_REQUEST;
                $datas = json_encode($exception->getErrors());
                break;
            case NotFoundHttpException::class:
                $statusCode = Response::HTTP_NOT_FOUND;
                $datas = json_encode(['message' => $exception->getMessage()]);
                break;
            case NotExportableEntityException::class:
                $statusCode = Response::HTTP_BAD_REQUEST;
                $datas = json_encode(['message' => $exception->getMessage()]);
        }

        $event->setResponse(new Response($datas, $statusCode, ['Content-Type' => 'application/json']));
    }
}
