<?php

namespace App\Exceptions;

class NotExportableEntityException extends \Exception
{
}
