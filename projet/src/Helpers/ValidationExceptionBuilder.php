<?php

namespace App\Helpers;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationExceptionBuilder
{
    public static function build(ConstraintViolationListInterface $violationList): array
    {
        $errors = [];
        foreach ($violationList as $violation) {
            $errors[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        return $errors;
    }
}
