<?php

namespace App\Helpers;

use App\Entity\Interfaces\ExportableEntityInterface;
use App\Exceptions\NotExportableEntityException;
use Symfony\Component\Serializer\SerializerInterface;

class ExportableEntityService
{
    public const ALLOWED_TYPES = ['historic', 'question'];

    protected SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function export($datas, string $delimiter = ';'): string
    {
        switch (gettype($datas)) {
            case 'array':
                if (
                    ($datas[0] instanceof ExportableEntityInterface &&
                    !$datas[0]->isExportable()) ||
                    !$datas[0] instanceof ExportableEntityInterface
                ) {
                    throw new NotExportableEntityException('Entity not exportable');
                }
                break;
            case 'object':
                if (
                    ($datas instanceof ExportableEntityInterface &&
                    !$datas->isExportable()) ||
                    !$datas instanceof ExportableEntityInterface
                ) {
                    throw new NotExportableEntityException('Entity Not exportable');
                }
                break;
        }

        return $this->serializer->serialize(
            $datas,
            'csv',
            [
                        'groups' => ['base', 'csv'],
                        'csv_delimiter' => $delimiter
                    ]
        );
    }
}
