<?php

namespace App\Controller;

use App\Entity\HistoricQuestion;
use App\Entity\Question;
use App\Exceptions\ValidationException;
use App\Helpers\ExportableEntityService;
use App\Helpers\ValidationExceptionBuilder;
use App\Inputs\QuestionInput;
use App\Repository\HistoricQuestionRepository;
use App\Repository\QuestionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class QuestionController
{
    protected SerializerInterface $serializer;

    protected ValidatorInterface $validator;

    protected EntityManagerInterface $entityManager;

    protected QuestionRepository $questionRepository;

    protected HistoricQuestionRepository $historicQuestionRepository;

    protected ExportableEntityService $exportableEntityService;

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager,
        QuestionRepository $questionRepository,
        HistoricQuestionRepository $historicQuestionRepository,
        ExportableEntityService $exportableEntityService
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->entityManager = $entityManager;
        $this->questionRepository = $questionRepository;
        $this->historicQuestionRepository = $historicQuestionRepository;
        $this->exportableEntityService = $exportableEntityService;
    }

    /**
     * @Route("/questions", name="create_question", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ValidationException
     */
    public function create(Request $request): Response
    {
        $requestDatas = $this->serializer->deserialize($request->getContent(), QuestionInput::class, 'json');

        $constraintList = $this->validator->validate($requestDatas, null, ['create']);
        if ($constraintList->count() > 0) {
            throw new ValidationException(ValidationExceptionBuilder::build($constraintList));
        }

        $question = Question::createFromInput($requestDatas);
        $this->entityManager->persist($question);
        $this->entityManager->flush();

        return new Response(
            $this->serializer->serialize($question, 'json', ['groups' => ['base', 'create_question']]),
            Response::HTTP_CREATED,
            [
                'Content-Type' => 'application/json'
            ]
        );
    }

    /**
     * @Route("/questions/{id}", name="update_question", methods={"PUT"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request): Response
    {
        $question = $this->questionRepository->find($request->attributes->get('id'));
        if (!$question instanceof Question) {
            throw new NotFoundHttpException("La question n'a pas été trouvé.");
        }
        $questionInput = $this->serializer->deserialize($request->getContent(), QuestionInput::class, 'json');
        $constraintList = $this->validator->validate(
            $questionInput,
            null,
            ['update']
        );
        if ($constraintList->count() > 0) {
            throw new ValidationException(ValidationExceptionBuilder::build($constraintList));
        }
        $question->updateFromInput($questionInput);
        $questionBeforeUpdate = $this->entityManager->getUnitOfWork()->getOriginalEntityData($question);
        $historic = HistoricQuestion::create(
            $question,
            $this->serializer->serialize($questionBeforeUpdate, 'json', ['groups' => ['base','update']]),
            $this->serializer->serialize($question, 'json', ['groups' => ['base','update']])
        );
        $this->entityManager->persist($historic);
        $this->entityManager->flush();

        return new JsonResponse(
            ['updated_at' => $question->getUpdatedAt()],
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/exports", name="export_csv", methods={"GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function exportFile(Request $request)
    {
        if (!$request->query->has('type')) {
            throw new ValidationException(
                ['message' => 'You must define type of datas targeted. Allow `question` or `historic`']
            );
        }
        if (!in_array($request->query->get('type'), ExportableEntityService::ALLOWED_TYPES)) {
            throw new ValidationException(
                ['message' => 'Type not allowed. Allow ' . implode(',', ExportableEntityService::ALLOWED_TYPES)]
            );
        }
        $datas = $request->query->get('type') === 'question' ?
            $this->questionRepository->findAll() :
            $this->historicQuestionRepository->findAll();

        $response = new Response(
            $this->exportableEntityService->export($datas),
            200,
            [
                'Content-Type' => 'text/csv'
            ]
        );
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            time() . '.csv'
        );
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }
}
