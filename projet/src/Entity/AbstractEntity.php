<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

abstract class AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(type="string")
     *
     * @Groups({"base"})
     */
    protected string $id;

    public function __construct()
    {
        $this->id = Uuid::v4()->__toString();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}
