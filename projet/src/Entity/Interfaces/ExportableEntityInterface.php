<?php

namespace App\Entity\Interfaces;

interface ExportableEntityInterface
{
    public function isExportable(): bool;
}
