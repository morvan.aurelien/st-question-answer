<?php

namespace App\Entity;

use App\Entity\Interfaces\ExportableEntityInterface;
use App\Inputs\AnswerInput;
use App\Inputs\QuestionInput;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\Repository\QuestionRepository")
 */
class Question extends AbstractEntity implements ExportableEntityInterface
{
    public const ALLOWED_STATUS = ['draft', 'published'];

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     *
     * @Groups({"create_question", "csv"})
     */
    protected \DateTime $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     *
     * @Groups({"create_question", "update", "csv"})
     */
    protected \DateTime $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     *
     * @Groups({"update", "csv"})
     */
    protected string $title;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @Groups({"csv"})
     */
    protected bool $promoted;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Groups({"update", "csv"})
     */
    protected string $status;

    /**
     * @var Collection|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Answer", mappedBy="question", cascade={"persist"})
     *
     * @Groups({"create_question", "csv"})
     */
    protected Collection $answers;

    public function __construct(string $title, bool $promoted, string $status)
    {
        $this->title = $title;
        $this->promoted = $promoted;
        $this->status = $status;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->answers = new ArrayCollection();
        parent::__construct();
    }

    public static function createFromInput(QuestionInput $requestDatas)
    {
        $question = new self($requestDatas->getTitle(), $requestDatas->getPromoted(), $requestDatas->getStatus());

        array_map(function (AnswerInput $answer) use ($question) {
            $question->addAnswer(new Answer($question, $answer->getChannel(), $answer->getChannel()));
        }, $requestDatas->getAnswers());

        return $question;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isPromoted(): bool
    {
        return $this->promoted;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    public function addAnswer(Answer $answer)
    {
        if (!$this->answers->contains($answer)) {
            $this->answers->add($answer);
        }
    }

    public function updateFromInput(QuestionInput $questionInput)
    {
        if (strlen($questionInput->getTitle()) > 0 && $questionInput->getTitle() !== $this->title) {
            $this->title = $questionInput->getTitle();
        }
        if (strlen($questionInput->getStatus()) > 0 && $questionInput->getStatus() !== $this->status) {
            $this->status = $questionInput->getStatus();
        }
        $this->updatedAt = new \DateTime();
    }

    public function isExportable(): bool
    {
        return true;
    }
}
