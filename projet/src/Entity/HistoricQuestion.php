<?php

namespace App\Entity;

use App\Entity\Interfaces\ExportableEntityInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\Repository\HistoricQuestionRepository")
 */
class HistoricQuestion extends AbstractEntity implements ExportableEntityInterface
{
    /**
     * @var Question
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Question")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     *
     * @Groups({"csv"})
     */
    protected Question $question;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     *
     * @Groups({"csv"})
     */
    protected \DateTime $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(type="json")
     *
     * @Groups({"csv"})
     */
    protected string $lastDatas;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Groups({"csv"})
     */
    protected string $newDatas;

    public function __construct(Question $question, string $lastDatas, string $newDatas)
    {
        $this->question = $question;
        $this->lastDatas = $lastDatas;
        $this->newDatas = $newDatas;
        $this->createdAt = new \DateTime();
        parent::__construct();
    }

    public static function create(Question $question, string $lastDatas, string $newDatas)
    {
        return new self($question, $lastDatas, $newDatas);
    }

    /**
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getLastDatas(): string
    {
        return $this->lastDatas;
    }

    /**
     * @return string
     */
    public function getNewDatas(): string
    {
        return $this->newDatas;
    }

    public function isExportable(): bool
    {
        return true;
    }
}
