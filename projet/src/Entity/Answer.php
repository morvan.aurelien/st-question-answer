<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class Answer extends AbstractEntity
{
    public const ALLOWED_CHANNEL = ['faq', 'bot'];

    /**
     * @var Question
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Question", inversedBy="answers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    protected Question $question;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Groups({"csv"})
     */
    protected string $channel;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=500)
     *
     * @Groups({"csv"})
     */
    protected string $body;

    public function __construct(Question $question, string $channel, string $body)
    {
        $this->question = $question;
        $this->channel = $channel;
        $this->body = $body;
        parent::__construct();
    }

    /**
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }

    /**
     * @return string
     */
    public function getChannel(): string
    {
        return $this->channel;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }
}
