# Q&A - Smart Tribune - Backend - Coding test

## Documentations
[Specs API](https://app.swaggerhub.com/apis/amorvan/SmartTribune/1.0.0)
- Find MCD into `docs` folder

## Run project
1. This project runs under docker.
2. Go to `docker` folder and just launch this command `docker-compose up -d --build`
3. Copy file `.env` to `.env.local`
4. A Makefile in docker folder provider a lot of command to interact with differents containers & symfony project. Just launch `make` to list all commands
5. Launch `make test-unit` from docker folder to run unit test

## Workflow git & development
- Each development has been done in relation to an issue.
- Branch naming : `feature/*` for a feature development, `docs/*` related documentation
- Any branch has been merged to master with a pull request after successful Gitlab CI
- Any development respect PSR 1 & 12 for coding style

## Continuous Integration :
- A CI has been set up.
- On each branch push or merge to master, Gitlab CI check if all the tests are successful.

## Explain many technical choices
- For creation HistoricQuestion entity on update, I chose not to use Doctrine Listener because it's not a BP Doctrine.
- To population HistoricQuestion asynchronously, we can use the Messenger component with his Bus System.
